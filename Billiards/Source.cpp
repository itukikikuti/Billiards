#include "XLibrary11.hpp"
using namespace XLibrary11;

int Main()
{
	Camera camera;
	camera.color = Float4(0.0f, 0.5f, 0.0f, 0.0f);

	Sprite ballSprite(L"ball.png");

	//Float3 ball1Position;
	//Float3 ball1Speed;

	//Float3 ball2Position;
	//Float3 ball2Speed;

	Float3 ballPosition[11];
	Float3 ballSpeed[11];

	ballPosition[0] = Float3(100.0f, 0.0f, 0.0f);
	ballPosition[1] = Float3(-100.0f, 0.0f, 0.0f);
	ballPosition[2] = Float3(-116.0f, 8.0f, 0.0f);
	ballPosition[3] = Float3(-116.0f, -8.0f, 0.0f);
	ballPosition[4] = Float3(-132.0f, 16.0f, 0.0f);
	ballPosition[5] = Float3(-132.0f, 0.0f, 0.0f);
	ballPosition[6] = Float3(-132.0f, -16.0f, 0.0f);
	ballPosition[7] = Float3(-148.0f, 24.0f, 0.0f);
	ballPosition[8] = Float3(-148.0f, 8.0f, 0.0f);
	ballPosition[9] = Float3(-148.0f, -8.0f, 0.0f);
	ballPosition[10] = Float3(-148.0f, -24.0f, 0.0f);

	while (Refresh())
	{
		camera.Update();

		if (Input::GetKeyDown(VK_LBUTTON))
		{
			//ball1Speed = (Input::GetMousePosition() - ball1Position) * 0.1f;
			ballSpeed[0] = (Input::GetMousePosition() - ballPosition[0]) * 0.1f;
		}

		for (int i = 0; i < 11; i++)
		{
			ballSpeed[i] *= 0.99f;
			ballPosition[i] += ballSpeed[i];

			for (int j = 0; j < 11; j++)
			{
				if (i == j)
				{
					continue;
				}

				Float3 a = ballPosition[i];
				Float3 b = ballPosition[j];
				if (sqrtf(powf(a.x - b.x, 2) + powf(a.y - b.y, 2)) < 16.0f)
				{
					ballSpeed[i] += (ballPosition[i] - ballPosition[j]) * 0.1f;
					ballSpeed[j] += (ballPosition[j] - ballPosition[i]) * 0.1f;
				}
			}

			if (ballPosition[i].x < -320.0f)
			{
				ballPosition[i].x = -320.0f;
				ballSpeed[i].x = -ballSpeed[i].x;
			}
			if (ballPosition[i].x > 320.0f)
			{
				ballPosition[i].x = 320.0f;
				ballSpeed[i].x = -ballSpeed[i].x;
			}
			if (ballPosition[i].y < -240.0f)
			{
				ballPosition[i].y = -240.0f;
				ballSpeed[i].y = -ballSpeed[i].y;
			}
			if (ballPosition[i].y > 240.0f)
			{
				ballPosition[i].y = 240.0f;
				ballSpeed[i].y = -ballSpeed[i].y;
			}

			ballSprite.position = ballPosition[i];
			ballSprite.Draw();
		}

		//ball1Speed *= 0.99f;
		//ball1Position += ball1Speed;

		//Float3 a = ball1Position;
		//Float3 b = ball2Position;
		//if (sqrtf(powf(a.x - b.x, 2) + powf(a.y - b.y, 2)) < 16.0f)
		//{
		//	ball1Speed += (ball1Position - ball2Position) * 0.1f;
		//	ball2Speed += (ball2Position - ball1Position) * 0.1f;
		//}

		//if (ball1Position.x < -320.0f)
		//{
		//	ball1Position.x = -320.0f;
		//	ball1Speed.x = -ball1Speed.x;
		//}
		//if (ball1Position.x > 320.0f)
		//{
		//	ball1Position.x = 320.0f;
		//	ball1Speed.x = -ball1Speed.x;
		//}
		//if (ball1Position.y < -240.0f)
		//{
		//	ball1Position.y = -240.0f;
		//	ball1Speed.y = -ball1Speed.y;
		//}
		//if (ball1Position.y > 240.0f)
		//{
		//	ball1Position.y = 240.0f;
		//	ball1Speed.y = -ball1Speed.y;
		//}

		//ballSprite.position = ball1Position;
		//ballSprite.Draw();

		//ball2Speed *= 0.99f;
		//ball2Position += ball2Speed;

		//if (ball2Position.x < -320.0f)
		//{
		//	ball2Position.x = -320.0f;
		//	ball2Speed.x = -ball2Speed.x;
		//}
		//if (ball2Position.x > 320.0f)
		//{
		//	ball2Position.x = 320.0f;
		//	ball2Speed.x = -ball2Speed.x;
		//}
		//if (ball2Position.y < -240.0f)
		//{
		//	ball2Position.y = -240.0f;
		//	ball2Speed.y = -ball2Speed.y;
		//}
		//if (ball2Position.y > 240.0f)
		//{
		//	ball2Position.y = 240.0f;
		//	ball2Speed.y = -ball2Speed.y;
		//}

		//ballSprite.position = ball2Position;
		//ballSprite.Draw();
	}
}
